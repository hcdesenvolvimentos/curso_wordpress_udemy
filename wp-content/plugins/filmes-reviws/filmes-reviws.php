<?php

/**
 * Plugin Name: Plugin Filmes Revews
 * Description: Controle base  Plugin Filmes Revews.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */

class Filmes_reviews{

	private static $instance;
	public static function getInstance(){
		if (self::$instance == NULL) {
			self::$instance = new self();
		}							
	}

	private function __construct(){

		add_action('init', 'Filmes_reviews::register_post_type');

	}

	public static function register_post_type(){

		register_post_type('filmes_reviews', array(
			
			'labels'            =>array(
				'name'          => 'Filmes Reviews',
				'singular_name' => 'Filme Review'
			),
			'description'       => 'Post para cadastro de Review',
			'supports'          => array(
				'title',
				'editor',
				'excerpt',
				'author',
				'revisions',
				'thumbnail',
				'comments',
				'custom-fields',
			),
			'public'             => true,
			'menu_position'      => 4,
		));
	}


	public static function rewrite_rules(){
		self::register_post_type();
		flush_rewrite_rules();
	}

}

Filmes_reviews::getInstance();


register_deactivation_hook( __FILE__,'flush_rewrite_rules');
register_activation_hook( __FILE__,'Filmes_reviews::rewrite_rules');






	