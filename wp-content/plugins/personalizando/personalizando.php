<?php

/**
 * Plugin Name: Plugin Personalizando
 * Description: Controle base  Plugin Personalizando.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */

class Plugin_Personalizando{

	private static $instance;

	public static function getInstance(){

		if (self::$instance == NULL) {

			self::$instance = new self();
		
		}							

	}


	private function __construct(){

		//Desativar a action welcome_panel
		remove_action('welcome_panel','wp_welcome_panel');
		
		add_action('welcome_panel', array($this,'welcome_panel'));
		add_action('admin_enqueue_scripts',array($this, 'add_css'));

	}


	function welcome_panel(){
	?>
		<div class="welcome-panel-content">
			<h3>Olá seja bem vindo!</h3>
			<p>Faça as alterações no menu ao lado.</p>
		</div>
	<?php
	}

	function add_css(){
		
		wp_register_style('plugin-personalizando', plugin_dir_url(__FILE__).'css/plugin-personalizando.css');
		wp_enqueue_style('plugin-personalizando');
	
	}

}

Plugin_Personalizando::getInstance();






	