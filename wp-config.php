<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_curso_wordpresss_udemy' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '/iH2!aZz&5|U3eOiG1df+4=HIa&u/!{M`yD.0d}2r8]?`X/`3wJAm3n!]vIX~N`f' );
define( 'SECURE_AUTH_KEY',  '6L,L Py7Y ?U9O}@KR#<pw,Z<k5^[9O-OJ]a%<Tlx0~j1yH;C=KJ+:gaY_c .;<d' );
define( 'LOGGED_IN_KEY',    'Q%LS?ZJV8@5]3{Vk4}8<+4V>L7}g] &gdmBE0z;w!bD%eba+w]0|}Yc&7+9{s?Vv' );
define( 'NONCE_KEY',        'oh`Qc0}d-wK[ttctz`iCx#..%Sbf1.,>D7hYQ_rj0dkZO6z3;8#=e7T%Yi|?1[1f' );
define( 'AUTH_SALT',        '}#N$Do0LD#44A]eBnKSWjyd}9L:/jrUo+tmn5m{xdo==3i>n5UC)|r2qiaAG5wSx' );
define( 'SECURE_AUTH_SALT', 'x{1/]3g o{3)95zvigL*#E(;sjAOY6cig&Fr/8Y*MC7=:^`Sv/$*8mqmX^RL@Ogt' );
define( 'LOGGED_IN_SALT',   'y>*~UyhMq)]gf<Xqw4Vi>2G/@jHE%ADBFN!=&[QPt=2sy)*8Mr6rQU:W)+GOa89K' );
define( 'NONCE_SALT',       'iQj`b~f;eQYWya pVxDPz!RU(l_[:ikBERv^-&Bf*1%!,`-@>aEDLATlPkC=XSqe' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'uy_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
